/*
    dcl-life, an implementation of "Game of Life" in Decentraland
    Copyright (C) 2019-2020  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* This is a Node.js script using gltf-js-utils to produce gltf models
   for the scene programmatically.  */

window = {};
const GLTFUtils = require ("gltf-js-utils");
const fs = require ("fs");

/**
 * Writes a set of glTF nodes to a model with the given directory name.
 */
function writeModel (dir, nodes)
{
  const asset = new GLTFUtils.GLTFAsset ();
  const scene = new GLTFUtils.Scene ();
  asset.addScene (scene);

  for (var i in nodes)
    scene.addNode (nodes[i]);

  GLTFUtils.exportGLTF (asset).then (function (fileData)
    {
      for (name in fileData)
        {
          const fullName = "models/" + dir + "/" + name;
          console.log ("Writing file: " + fullName);

          function logError (err)
            {
              if (err)
                console.log (err);
            }

          fs.writeFile (fullName, new Buffer (fileData[name]), logError);
        }
    });
}

/* ************************************************************************** */

/**
 * Constructs a node that corresponds to an invisible (collider-only) plane
 * that allows players to walk through them in one direction but not the other.
 * The plane will be from (0, 0, 0) to (1, 1, 0), and will allow access from
 * "negative" z to "positive" z but not the other way round.
 */
function OneWayPlaneNode ()
{
  const res = new GLTFUtils.Node ();
  res.name = "oneway_collider";
  res.mesh = new GLTFUtils.Mesh ();

  const vertices = [];
  for (let y = 0; y <= 1; ++y)
    for (let x = 0; x <= 1; ++x)
      {
        const v = new GLTFUtils.Vertex ();
        v.x = x;
        v.y = y;
        v.z = 0;
        v.normalX = 0;
        v.normalY = 0;
        v.normalZ = 1;
        vertices.push (v);
      }

  const m = new GLTFUtils.Material ();
  m.doubleSided = false;
  m.pbrMetallicRoughness.baseColorFactor = [1, 0, 0, 0];
  m.pbrMetallicRoughness.metallicFactor = 0;
  m.pbrMetallicRoughness.roughnessFactor = 1;
  res.mesh.material = [m];

  res.mesh.addFace (vertices[0], vertices[2], vertices[1], null, 0);
  res.mesh.addFace (vertices[1], vertices[2], vertices[3], null, 0);

  return res;
}

writeModel ("oneway", [OneWayPlaneNode ()]);
