# Decentraland Game-of-Life

This project implements a very simple (and crudely visualised)
[Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
as a scene for [Decentraland](https://decentraland.org/).

The scene contains a basic geometric "frame", which represents the empty grid
of cells.  For every cell that is alive, a 3D box is placed on that grid.
