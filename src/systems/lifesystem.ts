/*
    dcl-life, an implementation of "Game of Life" in Decentraland
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 * System implementing the "game-of-life" rules.  It keeps track of the alive
 * tile entities and adds/removes them as per the game rules.
 */
export default class LifeSystem implements ISystem
{

  numTiles: number;
  tileConstructor: (x: number, y: number) => Entity;

  tiles: (Entity | null)[][];
  hiddenTiles: (Entity | null)[][];

  stepLength: number | null
  accumulatedDt: number | null

  constructor (num: number, tc: (x: number, y: number) => Entity)
  {
    this.numTiles = num;
    this.tileConstructor = tc;
    this.tiles = [];
    this.hiddenTiles = [];
    for (let x: number = 0; x < this.numTiles; ++x)
      {
        this.tiles[x] = [];
        this.hiddenTiles[x] = [];
        for (let y: number = 0; y < this.numTiles; ++y)
          {
            this.tiles[x][y] = null;
            this.hiddenTiles[x][y] = null;
          }
      }
  }

  set (x: number, y: number, alive: boolean): void
  {
    if (alive)
      {
        if (this.tiles[x][y] != null)
          return;

        if (this.hiddenTiles[x][y] == null)
          this.tiles[x][y] = this.tileConstructor (x, y);
        else
          {
            this.tiles[x][y] = this.hiddenTiles[x][y];
            this.hiddenTiles[x][y] = null;
          }

        engine.addEntity (this.tiles[x][y]);
      }
    else
      {
        if (this.tiles[x][y] == null)
          return;

        engine.removeEntity (this.tiles[x][y]);
        this.hiddenTiles[x][y] = this.tiles[x][y];
        this.tiles[x][y] = null;
      }
  }

  get (x: number, y: number): boolean
  {
    x += this.numTiles;
    y += this.numTiles;
    return (this.tiles[x % this.numTiles][y % this.numTiles] != null);
  }

  aliveNeighbours (x: number, y: number): number
  {
    let res: number = 0;
    const steps: [number, number][] =
      [
        [-1, -1],
        [-1, 0],
        [-1, 1],
        [0, -1],
        [0, 1],
        [1, -1],
        [1, 0],
        [1, 1],
      ];
    for (const i in steps)
      if (this.get (x + steps[i][0], y + steps[i][1]))
        res += 1;

    return res;
  }

  start (stepLength: number): void
  {
    this.stepLength = stepLength;
    this.accumulatedDt = 0;
  }

  stop (): void
  {
    this.stepLength = null;
  }

  update (dt: number)
  {
    if (this.stepLength == null)
      return;

    this.accumulatedDt += dt;
    if (this.accumulatedDt < this.stepLength)
      return;

    this.accumulatedDt -= this.stepLength;
    let updates: [number, number, boolean][] = [];
    for (let x: number = 0; x < this.numTiles; ++x)
      for (let y: number = 0; y < this.numTiles; ++y)
        {
          const neighbours: number = this.aliveNeighbours (x, y);
          if (this.get (x, y))
            {
              if (neighbours < 2 || neighbours > 3)
                updates.push ([x, y, false]);
            }
          else
            {
              if (neighbours == 3)
                updates.push ([x, y, true]);
            }
        }

    log ("Number of updates: " + updates.length);
    for (const i in updates)
      this.set (updates[i][0], updates[i][1], updates[i][2]);
  }

}
