/*
    dcl-life, an implementation of "Game of Life" in Decentraland
    Copyright (C) 2019-2020  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import LifeSystem from "systems/lifesystem";

import { movePlayerTo } from "@decentraland/RestrictedActions";

/* Define the basic geometric parameters first.  We have four main component
   in our geometry:  The outer border, inner separators between tiles,
   the floor (visible for "dead" tiles) and the blocks we place on alive tiles.
   In addition, we have the play and stop button controls that hover over the
   mid of each of the four sides.

   The parameters define the basic sizes and materials for all of them.  */

const eps: number = 0.01;
const totalSize: number = 16;
const outerBorder: number = 0.5;
const outerHeight: number = 0.5;
const floorHeight: number = 0.1;
const separatorWidth: number = 0.1;
const separatorHeight: number = 0.2;
const numTiles: number = 10;
const tileHeight: number = 1;
const controlSize: number = 1;
const controlHeight: number = 1.5;
const controlSpacing: number = 0.5;
const topCeilingHeight: number = 6;
const ceilingBorder: number = 0.1;
const stepLength: number = 1;

const tileSize: number = (totalSize - 2 * outerBorder
                            - (numTiles - 1) * separatorWidth)
                          / numTiles;

const frameMaterial: Material = new Material ();
frameMaterial.albedoColor = Color3.Gray ();
frameMaterial.metallic = 0;
frameMaterial.roughness = 1;

const floorMaterial: Material = new Material ();
floorMaterial.albedoColor = Color3.Blue ();
floorMaterial.metallic = 0;
floorMaterial.roughness = 1;

const tileMaterial: Material = new Material ();
tileMaterial.albedoColor = Color3.Red ();
tileMaterial.metallic = 0;
tileMaterial.roughness = 1;

const ceilingBorderMaterial: Material = new Material ();
ceilingBorderMaterial.albedoColor = Color3.Black ();
ceilingBorderMaterial.metallic = 0;
ceilingBorderMaterial.roughness = 1;

const inactiveMaterial: Material = new Material ();
inactiveMaterial.albedoColor = Color3.Gray ();
inactiveMaterial.metallic = 0;
inactiveMaterial.roughness = 1;

const playMaterial: Material = new Material ();
playMaterial.albedoColor = Color3.Green ();
playMaterial.metallic = 0;
playMaterial.roughness = 1;

const stopMaterial: Material = new Material ();
stopMaterial.albedoColor = Color3.Yellow ();
stopMaterial.metallic = 0;
stopMaterial.roughness = 1;

/* We need to keep track of all the control shapes themselves, so that
   we can update their material as the system is stopped or started.  */
const playControls: Entity[] = [];
const stopControls: Entity[] = [];

let sys: LifeSystem;

/* ************************************************************************** */

const basicBox: BoxShape = new BoxShape ();
const basicCone: ConeShape = new ConeShape ();
const basicCylinder: CylinderShape = new CylinderShape ();
basicCylinder.radiusTop = 1;
basicCylinder.radiusBottom = 1;
basicCylinder.withCollisions = false;
const oneWayModel: GLTFShape = new GLTFShape ("models/oneway/model.gltf");
oneWayModel.isPointerBlocker = false;

/**
 * Constructs a generic box with given x/y dimensions and from the ground up
 * to the given height.  The result is returned as entity.
 */
function CreateBox (lower: [number, number], upper: [number, number],
                    height: number,
                    material: Material): Entity
{
  const xSize: number = upper[0] - lower[0];
  const ySize: number = upper[1] - lower[1];

  const res: Entity = new Entity ();
  res.addComponent (basicBox);
  res.addComponent (new Transform ());
  res.getComponent (Transform).scale.set (xSize, height, ySize);
  res.getComponent (Transform).position.set (lower[0] + xSize / 2, height / 2,
                                    lower[1] + ySize / 2);
  res.addComponent (material);

  return res;
}

/**
 * Computes and returns the coordinate (x or y) at which a tile block
 * with the given x/y index starts and ends.
 */
function TileCoord (n: number): [number, number]
{
  const lower: number = outerBorder + n * (tileSize + separatorWidth);
  return [lower, lower + tileSize];
}

/**
 * Given an x or y coordinate corresponding to "real space", return the
 * tile index this corresponds to.
 */
function TileIndexFromCoord (x: number): number | null
{
  const perTile: number = tileSize + separatorWidth;
  const ind: number = Math.floor ((x - outerBorder) / perTile);

  if (ind < 0 || ind >= numTiles)
    return null;

  return ind;
}

/**
 * Constructs and returns the Entity instance for a box representing the
 * alive tile at the given tile coordinates.
 */
function TileBox (x: number, y: number): Entity
{
  const xCoords: [number, number] = TileCoord (x);
  const yCoords: [number, number] = TileCoord (y);

  const res: Entity = CreateBox ([xCoords[0], yCoords[0]],
                                 [xCoords[1], yCoords[1]],
                                 tileHeight, tileMaterial);

  res.addComponent (new OnPointerDown (e => {
    log ("Disabling cell at (" + x + ", " + y + ")");
    sys.set (x, y, false);
  }, {hoverText: "remove"}));

  return res;
}

/**
 * Constructs a one-way transparent ceiling entity, including visible
 * border.  The constructed ceiling is at the given height, and returned
 * as entity.
 */
function CeilingGroup (height: number, withBorder: boolean): Entity
{
  const res: Entity = new Entity ();

  const plane: Entity = new Entity ();
  plane.addComponent (oneWayModel);
  plane.addComponent (new Transform ());
  plane.getComponent (Transform).scale.set (totalSize, totalSize, -1);
  plane.getComponent (Transform).rotation.setEuler (90, 0, 0);
  plane.getComponent (Transform).position.set (totalSize, 0, 0);
  plane.setParent (res);

  if (withBorder)
    for (let i = 0; i < 4; ++i)
      {
        const border: Entity = new Entity ();
        border.addComponent (basicCylinder);
        border.addComponent (ceilingBorderMaterial);
        const borderTransform: Transform = new Transform ();
        borderTransform.scale.set (ceilingBorder, totalSize / 2, ceilingBorder);
        borderTransform.rotation.setEuler (0, 0, 90);
        borderTransform.position.set (0, 0, ceilingBorder - totalSize / 2);
        border.addComponent (borderTransform);

        border.addComponent (new OnPointerDown (e => {
          movePlayerTo ({x: totalSize / 2, y: height + 1, z: totalSize / 2});
        }, {hoverText: "move up"}));

        const rotated: Entity = new Entity ();
        border.setParent (rotated);
        rotated.addComponent (new Transform ());
        rotated.getComponent (Transform).rotation.setEuler (0, 90 * i, 0);
        rotated.getComponent (Transform).position.set (totalSize / 2, 0,
                                                       totalSize / 2);

        rotated.setParent (res);
      }

  res.addComponent (new Transform ());
  res.getComponent (Transform).position.set (0, height, 0);

  return res;
}

/**
 * Constructs a button control, which will run the given function
 * when clicked.  It will use the given shape component and shift to the
 * left or right based on the sign.  This way, this function can both create
 * the "play" and "stop" buttons.
 */
function CreateControl (shape: Entity, sign: number, text: string,
                        action: () => void)
{
  shape.addComponent (inactiveMaterial);
  shape.addComponent (new OnPointerDown (e => {
    action ();
  }, {hoverText: text}));

  const res: Entity = new Entity;
  shape.setParent (res);
  res.addComponent (new Transform ());
  res.getComponent (Transform).scale.setAll (controlSize);
  res.getComponent (Transform).position.set (
      sign * (controlSize / 2 + controlSpacing),
      controlHeight + controlSize / 2,
      -totalSize / 2 + controlSize / 2
  );

  return res;
}

/**
 * Constructs a "play" button control.
 */
function CreatePlayControl (action: () => void)
{
  const shape: Entity = new Entity;
  shape.addComponent (basicCone);
  shape.addComponent (new Transform ());
  shape.getComponent (Transform).scale.setAll (0.5);
  shape.getComponent (Transform).rotation.setEuler (0, 0, -90);

  playControls.push (shape);

  return CreateControl (shape, -1, "play", action);
}

/**
 * Constructs a "stop" button control.
 */
function CreateStopControl (action: () => void)
{
  const shape: Entity = new Entity;
  shape.addComponent (basicBox);
  shape.addComponent (new Transform ());

  stopControls.push (shape);

  return CreateControl (shape, 1, "pause", action);
}

/**
 * Updates the controls for the given started-state of the system.
 */
function UpdateControlsForSystemState (started: boolean)
{
  for (const i in playControls)
    playControls[i].addComponentOrReplace (
        started ? inactiveMaterial : playMaterial);

  for (const i in stopControls)
    stopControls[i].addComponentOrReplace (
        started ? stopMaterial : inactiveMaterial);
}

/* ************************************************************************** */

/* Build up the basic frame of our scene.  */
engine.addEntity (CreateBox ([0, 0], [totalSize, outerBorder], outerHeight,
                             frameMaterial));
engine.addEntity (CreateBox ([0, 0], [outerBorder, totalSize], outerHeight,
                             frameMaterial));
engine.addEntity (CreateBox ([totalSize - outerBorder, 0],
                             [totalSize, totalSize], outerHeight,
                             frameMaterial));
engine.addEntity (CreateBox ([0, totalSize - outerBorder],
                             [totalSize, totalSize], outerHeight,
                             frameMaterial));

const floor: Entity = CreateBox ([eps, eps], [totalSize - eps, totalSize - eps],
                                 floorHeight, floorMaterial);
floor.addComponent (new OnPointerDown (e => {
  const x: number = e.hit.hitPoint.x;
  const y: number = e.hit.hitPoint.z;
  log ("Floor clicked at (" + x + ", " + y + ")");

  const nx: number = TileIndexFromCoord (x);
  const ny: number = TileIndexFromCoord (y);
  if (nx == null || ny == null)
    {
      log ("Click does not correspond to a tile");
      return;
    }

  log ("Enabling tile (" + nx + ", " + ny + ")");
  sys.set (nx, ny, true);
}, {hoverText: "create"}));
engine.addEntity (floor);

for (let i: number = 1; i < numTiles; ++i)
  {
    const pos: [number, number] = TileCoord (i);
    engine.addEntity (CreateBox ([pos[0] - separatorWidth, eps],
                                 [pos[0], totalSize - eps],
                                 separatorHeight, frameMaterial));
    engine.addEntity (CreateBox ([eps, pos[0] - separatorWidth],
                                 [totalSize - eps, pos[0]],
                                 separatorHeight, frameMaterial));
  }

/* Add a transparent plane "above", which will allow players to
   observe the scene from above.  */
engine.addEntity (CeilingGroup (topCeilingHeight, true));

/* Set up the life system.  */
sys = new LifeSystem (numTiles, TileBox);
engine.addSystem (sys);

/* Set up the controls.  */
for (let i: number = 0; i < 4; ++i)
  {
    const play: Entity = CreatePlayControl (() => {
      log ("Play clicked");
      UpdateControlsForSystemState (true);
      sys.start (stepLength);
    });

    const stop: Entity = CreateStopControl (() => {
      log ("Stop clicked");
      UpdateControlsForSystemState (false);
      sys.stop ();
    });

    const outer: Entity = new Entity;
    outer.addComponent (new Transform ());
    outer.getComponent (Transform).position.set (
        totalSize / 2,
        0,
        totalSize / 2
    );
    outer.getComponent (Transform).rotation.setEuler (0, i * 90, 0);

    play.setParent (outer);
    stop.setParent (outer);
    engine.addEntity (outer);
  }

/* ************************************************************************** */

/* Initial configuration: blinker  */
/*
sys.set (2, 3, true);
sys.set (3, 3, true);
sys.set (4, 3, true);
*/

/* Initial configuration: glider  */
sys.set (0, 0, true);
sys.set (0, 1, true);
sys.set (0, 2, true);
sys.set (1, 2, true);
sys.set (2, 1, true);

UpdateControlsForSystemState (true);
sys.start (stepLength);
